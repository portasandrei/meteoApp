"use strict";

var cron = require('node-cron');
const Meteo = require('./api');

var meteo = new Meteo();


class CronMeteo{
	runCron(){
		cron.schedule('2 * * * * *', function(){
			meteo.getMeteoData();
		});
	}
	
} 

module.exports = CronMeteo;